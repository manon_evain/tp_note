import requests
from random import randint

from serveur.chanson import Chanson

def choix_artiste(liste):
    nb_artiste = len(liste)
    note_total = 0
    intervale = [0]
    for i in range(0, nb_artiste):
        note_i = liste[i]["note"]
        note_total += note_i
        intervale.append(intervale[i] + note_i)
    nb_random = randint(0, note_total)
    ind = 0
    a = intervale[ind]
    b = intervale[ind+1]
    while (nb_random >= a and nb_random <= b) == False: 
        ind = ind+1
        a = intervale[ind]
        b = intervale[ind+1]
    nom_artiste = liste[ind]["artiste"]
    return nom_artiste
    
def chanson_egales(chanson1 : Chanson, chanson2 : Chanson):
    return chanson1['artist'] == chanson2['artist'] and chanson1['title'] == chanson2['title']

def chanson_in_liste(liste : list, chanson : Chanson, taille): 
    r = False
    for i in range(0, taille): 
        if chanson_egales(liste[i], chanson) : 
            r = True
    return r 

def liste_chanson_function(liste, nb_chanson):
    liste_chanson = [] 
    taille = 0
    i = 0
    while i < nb_chanson :
        artiste = choix_artiste(liste)
        try :
            r  = requests.get("http://localhost:5000/random/" + artiste)
            chanson = r.json()
            if chanson != None : 
                if chanson_in_liste(liste_chanson, chanson, i) == False:
                    liste_chanson.append(chanson)
                    i += 1
        except : 
            None
    return liste_chanson

