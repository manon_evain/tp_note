import requests
from random import randint

#Récupération de l'id de l'artiste
def id_artiste_fun(nom_artiste):
    url_artist = "https://www.theaudiodb.com/api/v1/json/2/search.php?s="+nom_artiste
    x_artist = requests.get(url_artist)
    print(x_artist)
    reponse_artist = x_artist.json()
    id_artist = reponse_artist["artists"][0]["idArtist"]
    return id_artist

def id_album_fun(id_artiste): 
    url_album = "https://theaudiodb.com/api/v1/json/2/album.php?i="+id_artiste
    x_album = requests.get(url_album)
    reponse_album = x_album.json()
    list_album = reponse_album["album"]
    nb_album = len(list_album)
    nb_random = randint(0, nb_album - 1)
    id_album = list_album[nb_random]["idAlbum"]
    return id_album

def info_chanson_fun(id_album):
    #Récupération de l'id d'une chanson aléatoire de l'album de l'artiste
    url_tracks = "https://theaudiodb.com/api/v1/json/2/track.php?m="+id_album
    x_track = requests.get(url_tracks)
    reponse_track = x_track.json()
    list_track = reponse_track["track"]
    nb_track = len(list_track)
    nb_random2 = randint(0, nb_track - 1)

    #Récupération du titre de la chanson 
    title = list_track[nb_random2]["strTrack"]

    link_ytb = list_track[nb_random2]["strMusicVid"]
    if link_ytb == None :
        link_ytb = "Il n'y a pas de lien youtube pour cette chanson"
    
    return [title, link_ytb]

def lyrics_fun(title, nom_artiste):
    url_lyrics = "https://api.lyrics.ovh/v1/" + nom_artiste + "/" + title
    x_lyrics = requests.get(url_lyrics)
    reponse_lyrics = x_lyrics.json()
    lyrics = reponse_lyrics["lyrics"]
    return lyrics

