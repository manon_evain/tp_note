import uvicorn
from fastapi import FastAPI

from serveur.chanson import Chanson
from serveur.fonction import id_artiste_fun, id_album_fun, info_chanson_fun, lyrics_fun

app = FastAPI()

@app.get("/")
def read_root():
    return {"Vérification": "L'API fonctionne correctement"}

@app.get("/random/{nom_artiste}")
def get_musique(nom_artiste : str):
    #Récupération des infos de la chanson aléatoire
    id_artist = id_artiste_fun(nom_artiste)
    id_album = id_album_fun(id_artist)
    title = info_chanson_fun(id_album)[0]
    link_ytb = info_chanson_fun(id_album)[1]
    lyrics = lyrics_fun(title, nom_artiste)

    #Création de la chanson 
    chanson = Chanson(nom_artiste, title, link_ytb, lyrics)
    return chanson

if __name__ == "__main__" :
    uvicorn.run(app, host="localhost", port = "5000", log_level="info")
