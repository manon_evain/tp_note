from pydantic import BaseModel

class Chanson(BaseModel):
    artist : str
    title : str 
    suggested_youtube_url : str 
    lyrics : str
    
    def __init__(self, artist : str, title : str, suggested_youtube_url : str, lyrics : str):
        super().__init__(artist = artist, title = title, suggested_youtube_url = suggested_youtube_url, lyrics = lyrics)

    def __str__(self):
        return {"artist :" + self.artist + "," + '\n'
                "title :" + self.title + ","  + '\n'
                "suggested_youtube_url :" + self.suggested_youtube_url + "," + '\n'
                "lyrics :" + self.lyrics}

