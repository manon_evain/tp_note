import unittest
from unittest import TestCase

from client.scenario import choix_artiste
from serveur.fonction import id_artiste_fun

class TestPlaylist(TestCase) : 
    def test_Artiste(self): 
        liste = [{
            "artiste": "daft punk",
            "note": 18
        },
        {
            "artiste":"gloria gaynor",
            "note": 10
        },
        {
            "artiste":"boney m",
            "note": 5
        },
        {
            "artiste":"oasis",
            "note": 20
        }]
        artiste_random = choix_artiste(liste)
        liste_artiste = ["oasis", "boney m","gloria gaynor", "daft punk"]

        self.assertIn(artiste_random, liste_artiste)

    
    def testIdArtiste(self):
        id_artiste = id_artiste_fun("Daft Punk")

        self.assertEquals(id_artiste, "111492")
        
if __name__ == '__main__':
    unittest.main()