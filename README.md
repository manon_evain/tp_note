# Conception de logiciel - TP noté
Auteur du projet : EVAIN Manon

## Description générale du projet 

Ce projet à pour objectif de créer une API musicale basée sur des API cibles déjà existantes. 

Il présente également une utilisation de cette API, qui permet de créer un playlist de 20 musique à partir d'un fichier qui donne les artistes préférés de la personne souhaitant utilisé cette API. 

Le projet est divisé en trois dossiers :  
- client : La partie du code relative au client qui accède à l'API du serveur  
- serveur : La partie du code relative au serveur, le client n'a pas besoin de coder dans le dossier serveur afin de lancer l'API, le code existant se suffit à lui même  
- test_unitaire : Ce dossier contient les tests unitaires pour deux fonctions du projet   

***Remarques :*** Attention, lors de l'ensemble des commandes, sur ma machine il faut utiliser la version 3.9, c'est à dire, chaque commandes python et pip sont remplacées respectivement par python3.9 et pip3.9.  

## QuickStart

Afin d'utiliser ce projet, voici les différentes étapes à réaliser pour pouvoir utilisé ce dépot.

- Téléchargement du dépot :   
Clonez le dépot Git à partir du lien suivant : "https://gitlab.com/manon_evain/tp_note.git"  
- Installation des libraries utiles pour ce projet :   
Pour le coté client, veuillez utilisé les commandes suivantes :   
-> cd client  
-> pip install requierements.txt  
-> cd ..   
Pour le coté serveur, veuillez utilisé les commandes suivantes :   
-> cd serveur   
-> pip install requierements.txt  
-> cd ..     
- Lancement de l'api :  
Pour lancer l'API, il faut lancer les commandes suivantes :   
-> cd serveur   
-> python main.py  

## Lancement du scénario

Afin de lancer le scénario, c'est à dire d'obtenir la liste des 20 chansons obtenues aléatoirement à partir du fichier de type rudy (préférences des chanteurs d'une personne), voici les différentes étapes à réaliser. 

Tout d'abord, vous devez couper votre terminal en deux partie.   
- Commande dans le premier serveur : Lancement de l'API    
-> cd serveur    
-> python main.py     
- Commande dans le second serveur : Le scénario    
-> cd client    
-> python main.py  

## Lancement des tests unitaires 

Dans ce dossier, deux fonctions sont testées à travers de tests unitaires :   
- La fonction id_artist_fun du fichier serveur.fonction  
- La fonction choix_artiste du fichier client.scenario  

Afin de lancer les tests unitaires, il faut lancer dans le terminal les instructions suivantes :   
-> cd test_unitaire  
-> python test.py  

## Schéma d'architecture

```mermaid
graph TD;
    Client --> Serveur;
    Serveur --> AudioDB;
    Serveur --> LyricsOvh;
```